# InApp Assignments

## Assignments for Freshers' Training by InApp during November-December '20.

### Assignment 1: Problem Statement

```bash
Make a two-player `Rock-Paper-Scissors game`. One of the players is the computer.

10 rounds. Print out the winner and points earned by both players at the end of the game.

Remember the rules:

Rock beats Scissors
Scissors beats Paper
Paper beats Rock

Use a dictionary to store the history of choices made by the Player and the Computer.

You should be able to print the choices made by the Player and the Computer in each round once the game is completed.
```

#### Example

```bash
Enter the round for which you need the information >> 3

Output
`Player choice = Rock`
`Computer choice = Paper`
`Player won Round 3`
```

​                                               

