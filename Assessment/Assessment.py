#class pet
class Pet:
    def __init__(self,species=None,name=""):
        self.species = species
        self.name=name
        try:
            if species not in ['Dog','Cat','Horse','Hamster']:
                raise ValueError
        except ValueError:
               print("-----------------------------------------------------------")
               print('Species of {}'.format(self.species))
               print("ValueError: Species not defined in [Dog,Cat,Horse,Hamster]")
        finally:
            print("-----------------------------------------------------------")
    def __str__(self):
        if self.name :
            return "Species of {}, named {}".format(self.species,self.name)
        else:
            return "Species of {}, unnamed".format(self.species)
#child class Dog
class Dog(Pet):
    def __init__(self,name='',chases='Cats'):
        Pet.__init__(self,"Dog",name)
        self.chases = chases
    def __str__(self):
        if self.name :
            return "Species of {}, named {}, chases {}".format(self.species,self.name,self.chases)
        else:
            return "Species of {}, unnamed, chases {}".format(self.species,self.chases)
#child class cat
class Cat(Pet):
    def __init__(self, name, hates='Dogs'):
        Pet.__init__(self, "Cat", name)
        self.hates = hates
    def __str__(self):
        if self.name:
            return "Species of {}, named {}, hates {}".format(self.species, self.name, self.hates)
        else:
            return "Species of {}, unnamed, hates {}".format(self.species, self.hates)
#class main for objects
class main():
    Peter = Cat('Peter')
    print("Species is {}".format(Peter.species))
    print("{} hates {}.".format(Peter.name,Peter.hates))
    print(Peter)

    Unnamed_cat = Cat('')
    print("Species is {}".format(Unnamed_cat.species))
    print("Unnamed hates {}.".format(Unnamed_cat.hates))
    print(Unnamed_cat)

    Chuchu = Cat('Chuchu','Parrots')
    print("Species is {}".format(Chuchu.species))
    print("{} hates {}.".format(Chuchu.name, Chuchu.hates))
    print(Chuchu)

    Peppy = Dog('Peter')
    print("Species is {}".format(Peppy.species))
    print("{} chases {}.".format(Peppy.name, Peppy.chases))
    print(Peppy)

    Unnamed_dog = Dog('')
    print("Species is {}".format(Unnamed_dog.species))
    print("Unnamed chases {}.".format(Unnamed_dog.chases))
    print(Unnamed_dog)

    Chacha = Dog('Chacha', 'Crows')
    print("Species is {}".format(Chacha.species))
    print("{} chases {}.".format(Chacha.name, Chacha.chases))
    print(Chacha)

    not_in_species = Pet('crocodiles')
