#for generating random choices by computer
import random
import time
start = time.time()
#initialize
player_count,computer_count,tie_count = 0,0,0
choices = (0,1,2)

#possibilities dictionary
possibilities = {1: {1:4,0:3,2:5},0: {0:4,1:5,2:3},2:{2:4,0:5,1:3},'history':{'player':[],'computer':[],'final':[]}}

#appending for each iteration and updating counts,moves etc
for i in range(10):
    player_inputs = input("Enter your move (rock--0/paper--1/scissors--2): ")
    possibilities['history']['player'].append(player_inputs)
    computer_moves = random.choice(choices)
    possibilities['history']['computer'].append(computer_moves)
    print('Computer Shows (rock--0/paper--1/scissors--2): ' + str(computer_moves))
    result = possibilities[int(player_inputs)][int(computer_moves)]
    if result == 3:
        player_count += 1
        possibilities['history']['final'].append("Player Won")
        print("Player Won")
    elif result == 5:
        computer_count += 1
        possibilities['history']['final'].append("Player Lose")
        print("Player Lose")
    else :
        tie_count += 1
        possibilities['history']['final'].append("Tie")
        print("Tie")

#determining who won
if player_count > computer_count:
    print('Player Won the Game and the score is ' + str(player_count))
elif player_count < computer_count:
     print('Computer Won the Game and the score is: ' + str(computer_count))
else:
    print('Game Tied Between Player and Computer, the tie is:' + str(tie_count))
print('Game Summary --> Player:' + str(player_count) + ' Computer:' + str(computer_count) + ' Tie:' + str(tie_count))

#for printing history
history_info = input("Enter the round for which you need information: ")
print("Player Choice = " + str(possibilities['history']['player'][int(history_info)-1]))
print("Computer Choice = " + str(possibilities['history']['computer'][int(history_info)-1]))
print(possibilities['history']['final'][int(history_info)-1] + ' for round ' + str((int(history_info))))
end = time.time()
print(end - start)