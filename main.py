#for generating random choices by computer
import random
#initialize
player =[]
computer = []
final_result=[]
player_count = 0
computer_count = 0
tie_count = 0
choices = ['rock','paper','scissors']
history = {'player_history':player,'computer_history':computer,'won/lose':final_result}
#possibilities dictionary
possibilities = {'paper': {'paper':'Tie','rock':'Player Won','scissors':'Player Lose'},'rock': {'rock':'Tie','paper':'Player Lose','scissors':'Player Won'},'scissors':{'scissors':'Tie','rock':'Player Lose','paper':'Player Won'}}
#appending for each iteration and updating counts,moves etc
for i in range(10):
    player_inputs = input("Enter your move (rock/paper/scissors): ")
    player.append(player_inputs)
    computer_moves = random.choice(choices)
    computer.append(computer_moves)
    result = possibilities[player_inputs][computer_moves]
    final_result.append(result)
    print('Computer Shows: ' + computer_moves)
    print(result)
    if result == 'Player Won':
        player_count += 1
    elif result == 'Player Lose':
        computer_count += 1
    else :
        tie_count += 1
#determining who won
if player_count > computer_count:
    print('Player Won the Game and the score is ' + str(player_count))
elif player_count < computer_count:
     print('Computer Won the Game and the score is: ' + str(computer_count))
else:
    print('Game Tied Between Player and Computer, the tie is:' + str(tie_count))
print('Game Summary --> Player:' + str(player_count) + ' Computer:' + str(computer_count) + ' Tie:' + str(tie_count))
#for printing history
history_info = input("Enter the round for which you need information: ")
print("Player Choice = " + history['player_history'][int(history_info)-1])
print("Computer Choice = " + history['computer_history'][int(history_info)-1])
print(final_result[int(history_info)-1] + ' for round ' + str((int(history_info))))