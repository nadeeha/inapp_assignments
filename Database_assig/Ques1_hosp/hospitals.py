from db_utils import db_connect, create_hospital, create_doctor
con = db_connect() # connect to the database
cur = con.cursor() # instantiate a cursor obj
cur.execute("DROP TABLE IF EXISTS hospitals")
cur.execute("DROP TABLE IF EXISTS doctors")
hospitals_sql = """
     CREATE TABLE hospitals (
     hospital_id integer PRIMARY KEY,
     hospital_name text NOT NULL,
     bed_count integer NOT NULL)
     """
cur.execute(hospitals_sql)
doctors_sql = """
     CREATE TABLE doctors (
     doctor_id integer PRIMARY KEY,
     doctor_name text NOT NULL,
     hospital_id integer NOT NULL,
     joining_date text NOT NULL,
     speciality text NOT NULL, 
     salary integer NOT NULL, 
     experience NULL,
     FOREIGN KEY (hospital_id) references hospitals (hospital_id))"""
cur.execute(doctors_sql)
hosp1 = create_hospital(con,1,'Mayo Clinic', 200)
hosp2 = create_hospital(con,2,'Cleveland Clinic', 400)
hosp3 = create_hospital(con,3,'John Hopkins', 1000)
hosp4 = create_hospital(con,4,'UCLA Medical Centre', 1500)
doc1=create_doctor(con,101,'David',1,'2005-02-10','Pediatric',40000,0)
doc2=create_doctor(con,102,'Michael',1,'2018-07-23','Oncologist',20000,0)
doc3=create_doctor(con,103,'Susan',2,'2016-05-19','Garnacologist',25000,0)
doc4=create_doctor(con,104,'Robert',2,'2017-12-28','Pediatric',28000,0)
doc5=create_doctor(con,105,'Linda',3,'2004-06-04','Garnacologist',42000,0)
doc6=create_doctor(con,106,'William',3,'2012-09-11','Dermatologist',30000,0)
doc7=create_doctor(con,107,'Richard',4,'2014-08-21','Garnacologist',32000,0)
doc8=create_doctor(con,108,'Karen',4,'2011-10-17','Radiologist',30000,0)
con.commit()
while True:
    a = input("please choose speciality:(Pediatric/Garnacologist/Oncologist/Dermalogist/Radiologist) : ")
    b = input("Enter the salary: ")
    c = input("Do you wish to see doctors above the salary range, below or equal? (above/below/equal): ")
    par = a,b
    if c == "above":
        sql_1="SELECT doctor_name from doctors where speciality = ? and salary > ? "
        cur.execute(sql_1,par)
    elif c == "below":
        sql_2 = "SELECT doctor_name from doctors where speciality = ? and salary < ? "
        cur.execute(sql_2, par)
    elif c == "equal":
        sql_3 = "SELECT doctor_name from doctors where speciality = ? and salary = ? "
        cur.execute(sql_3, par)
    records = cur.fetchall()
    for i in records:
        print(i[0] + " is a " + a + " with salary " + c + " " + b )
    id = input("Give a hosp id: (1,2,3,4):")
    sql_4 = "SELECT doctors.doctor_name,hospitals.hospital_name from doctors,hospitals where doctors.hospital_id = hospitals.hospital_id and doctors.hospital_id = ?"
    cur.execute(sql_4, id)
    records1=cur.fetchall()
    for i in records1:
        print("Doctor " + i[0] + " is from " + i[1] + "(hosp id {})".format(id))
    yes = input("do you want to ask again? y/n:")
    if yes == 'y':
        continue
    else:
        break
# commit and close
con.commit()
con.close()