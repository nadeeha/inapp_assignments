# db_utils.py
import os
import sqlite3

# create a default path to connect to and create (if necessary) a database
# called 'database.sqlite3' in the same directory as this script
DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'database.sqlite3')

def db_connect(db_path=DEFAULT_PATH):
    con = sqlite3.connect(db_path)
    return con

def create_hospital(con, hospital_id, hospital_name, bed_count):
    sql = """
        INSERT INTO hospitals (hospital_id, hospital_name, bed_count)
        VALUES (?, ?,?)
        """
    cur = con.cursor()
    cur.execute(sql, (hospital_id, hospital_name, bed_count))
    return cur.lastrowid

def create_doctor(con, doctor_id ,doctor_name ,hospital_id, joining_date ,speciality,salary,experience ):
    sql = """
        INSERT INTO doctors (doctor_id ,doctor_name ,hospital_id, joining_date ,speciality,salary,experience )
        VALUES (?, ?,?,?,?,?,?)
        """
    cur = con.cursor()
    cur.execute(sql, (doctor_id ,doctor_name ,hospital_id, joining_date ,speciality,salary,experience ))
    return cur.lastrowid

