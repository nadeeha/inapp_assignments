from db_game import db_connect
con = db_connect() # connect to the database
cur = con.cursor() # instantiate a cursor obj

sql1 = 'SELECT COUNT(*) FROM Teams'
cur.execute(sql1)
record1 = cur.fetchall()
print("a)Number of rows in Teams is: " + str(record1[0][0]))

sql2 = 'SELECT distinct Season from Teams'
cur.execute(sql2)
record2 = cur.fetchall()
row_year = []
print("\nb)Unique values in seasons" )
for i in record2:
    row_year.append(i[0])
print(row_year)
sql3 = 'SELECT MIN(StadiumCapacity) from Teams'
cur.execute(sql3)
record3 = cur.fetchall()
print("\nc)Smallest Stadium Capacity: " + str(record3[0][0]))
sql4 = 'SELECT MAX(StadiumCapacity) from Teams'
cur.execute(sql4)
record4 = cur.fetchall()
print("\nc)Largest Stadium Capacity: " + str(record4[0][0]))
sql5 = 'SELECT SUM(KaderHome) from Teams where Season = 2014'
cur.execute(sql5)
record5 = cur.fetchall()
print("\nd)Sum: " + str(record5[0][0]))
sql6 = 'SELECT AVG(FTHG) from Matches where HomeTeam = "Man United"'
cur.execute(sql6)
record6 = cur.fetchall()
print("\ne)Average: " + str(record6[0][0]))



