from db_game import db_connect
con = db_connect() # connect to the database
cur = con.cursor() # instantiate a cursor obj

sql1 = 'SELECT HomeTeam,AwayTeam from Matches where Season = 2015 and FTHG = 5'
cur.execute(sql1)
record1 = cur.fetchall()
print("\nb)Name of HomeTeam and AwayTeam played in 2015 and Full time home goals of 5")
for i in record1:
    print("Home Team: {}---------------------------Away Team: {}".format(i[0],i[1]))
sql2 = 'SELECT * from Matches where HomeTeam = "Arsenal" and FTR = "A"'
cur.execute(sql2)
record2 = cur.fetchall()
print("\nc)Details of matches where home team was Arsenal and FTR A")
for i in record2:
    print("MatchID:{} Div:{} Season: {} Date: {} Away Team: {} FTHG: {}  FTAG: {} ".format(i[0],i[1],i[2],i[3],i[5],i[6],i[7]))
sql3 = 'SELECT * from Matches where Season between 2012 and 2015  and AwayTeam = "Bayern Munich" and FTAG > 5'
cur.execute(sql3)
record3 = cur.fetchall()
print("\nd)Details of matches between 2012 and 2015 where away team was Bayern Munich and FTAG > 2")
for i in record3:
    print("MatchID:{} Div:{} Season: {} Date: {} Home Team: {} FTHG: {}  FTAG: {}  FTR: {} ".format(i[0], i[1], i[2], i[3], i[4],
                                                                                           i[6], i[7],i[8]))
sql4 = 'SELECT * from Matches where HomeTeam LIKE "a%" and AwayTeam LIKE "m%"'
cur.execute(sql4)
record4 = cur.fetchall()
print("\ne)Details of matches with home name starting with A and away team starting with M")
for i in record4:
    print("MatchID:{} Div:{} Season: {} Date: {} Home Team: {} AwayTeam: {} FTHG: {}  FTAG: {}  FTR: {} ".format(i[0], i[1], i[2], i[3], i[4],i[5],
                                                                                           i[6], i[7],i[8]))


