from db_game import db_connect
con = db_connect() # connect to the database
cur = con.cursor() # instantiate a cursor obj

sql1 = 'SELECT HomeTeam,FTHG,FTAG FROM Matches where Season = 2010 and HomeTeam = "Aachen" ORDER BY FTHG DESC'
cur.execute(sql1)
record1 = cur.fetchall()
print("------------a) Home team, FTHG,FTAG from matches table from 2010 season with Aachen home team result returned in descending order of num of home goals------------------")
for i in record1:
    print("Home Team: {}  FTHG: {} FTAG: {}".format(i[0],i[1],i[2]))
sql2 = 'SELECT hometeam, COUNT(FTR = "H") FROM Matches where Season=2016 GROUP BY hometeam ORDER By Count(HomeTeam) DESC;'
cur.execute(sql2)
record2 = cur.fetchall()
print("\n------------b)  Total number of home games each team won during the 2016 season in descending order of number of home games from the Matches table.------------------")
for i in record2:
    print("Home Team: {}  COUNT: {}".format(i[0],i[1]))

sql3 = 'SELECT * from Unique_Teams LIMIT 10'
cur.execute(sql3)
record3 = cur.fetchall()
print("\n------------c) The first ten rows from the Unique_Teams------------------")
for i in record3:
    print("Team Name: {}  Unique Team: ID {}".format(i[0],i[1]))


sql4="SELECT Teams_in_Matches.Match_ID,Teams_in_Matches.Unique_Team_ID,Unique_Teams.TeamName from Teams_in_Matches,Unique_Teams where Teams_in_Matches.Unique_Team_ID = Unique_Teams.Unique_Team_ID LIMIT 5 "
cur.execute(sql4)
record4=cur.fetchall()
print("\n------------d) 1.Match_ID and Unique_Team_ID with the corresponding Team_Name from the Unique_Teams and Teams_in_Matches tables------------------")
for i in record4:
    print("Match ID: {}  Unique Team ID: {}  Team Name: {}".format(i[0],i[1],i[2]))
sql5='''SELECT Teams_in_Matches.match_id, Teams_in_Matches.unique_team_id, Unique_Teams.teamname
FROM Teams_in_Matches
INNER JOIN Unique_Teams ON Teams_in_Matches.unique_team_id=Unique_Teams.unique_team_id LIMIT 5;'''
cur.execute(sql5)
record5=cur.fetchall()
print("\n------------d) 2.Match_ID and Unique_Team_ID with the corresponding Team_Name from the Unique_Teams and Teams_in_Matches tables------------------")
for i in record5:
    print("Match ID: {}  Unique Team ID: {}  Team Name: {}".format(i[0],i[1],i[2]))
sql6='''SELECT *
FROM Teams
INNER JOIN Unique_Teams
ON Teams.TeamName = Unique_Teams.TeamName LIMIT 10'''
cur.execute(sql6)
print("\n------------e)  Joins together the Unique_Teams data table and the Teams table, and returns the first 10 rows------------------")
print(cur.fetchall())
sql7='''SELECT Unique_Team_ID,Unique_Teams.TeamName,AvgAgeHome,Season,ForeignPlayersHome
FROM Teams
INNER JOIN Unique_Teams
ON Teams.TeamName = Unique_Teams.TeamName LIMIT 5'''
cur.execute(sql7)
print("\n------------f) Unique_Team_ID and TeamName from the Unique_Teams table and AvgAgeHome, Season and ForeignPlayersHome from the Teams table------------------")
record7=cur.fetchall()
for i in record7:
    print("Unique Team ID: {}  Team Name: {}  AvgAgeHome: {} Season:{} ForeignPlayersHome: {}".format(i[0],i[1],i[2],i[3],i[4]))
sql8='''SELECT TeamName,MAX(Match_ID),Teams_in_Matches.Unique_Team_ID
FROM Unique_Teams
INNER JOIN Teams_in_Matches
ON Unique_teams.Unique_Team_ID = Teams_in_Matches.Unique_Team_ID WHERE TeamName LIKE "%y" OR TeamName LIKE "%r" GROUP BY TeamName LIMIT 5'''
cur.execute(sql8)
record8=cur.fetchall()
print("\n------------g) Highest Match_ID for each team that ends in a “y” or a “r”, along with the maximum Match_ID, display the Unique_Team_ID from the Teams_in_Matches table and the TeamName from the Unique_Teams table.------------------")
for i in record8:
    print("TeamName: {}  Max of Match ID ID: {}  Unique Team ID: {}".format(i[0],i[1],i[2]))
