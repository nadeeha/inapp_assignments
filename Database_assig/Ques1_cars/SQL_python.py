from db_func import db_connect, create_cars
con = db_connect() # connect to the database
cur = con.cursor()

# drop query
cur.execute("DROP TABLE IF EXISTS CARS")
# create query
query = """CREATE TABLE CARS(
        ID INT PRIMARY KEY NOT NULL,
        NAME_CAR CHAR(20) NOT NULL, 
        OWNER CHAR(20))"""

cur.execute(query)
car1 = create_cars(con,1,'Maruti', 'Suzane')
car2 = create_cars(con,2,'Benz', 'Paul')
car3 = create_cars(con,3,'BMW', 'Amal')
car4 = create_cars(con,4,'Honda','Hashu')
car5 = create_cars(con,5,'Maruti', 'Sreethi')
car6 = create_cars(con,6,'Maruti', 'Seema')
car7 = create_cars(con,7,'Hyundai', 'Shia')
car8 = create_cars(con,8,'Jaguar', 'Smith')
car9 = create_cars(con,9,'Maruti', 'Sam')
car10 = create_cars(con,10,'Toyota', 'Sneha')
con.commit()
cursor = con.execute("SELECT * from CARS")
records = cursor.fetchall()
for row in records:
    print(row[1] + "---" + row[2])

# commit and close
con.commit
con.close()