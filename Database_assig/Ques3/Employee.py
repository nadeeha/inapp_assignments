from db_func_emp import db_connect, create_employee, create_department
con = db_connect() # connect to the database
cur = con.cursor() # instantiate a cursor obj
cur.execute("DROP TABLE IF EXISTS employee")
cur.execute("DROP TABLE IF EXISTS departments")

employee = """
     CREATE TABLE employee (
     ID integer PRIMARY KEY,
     NAME text NOT NULL,
     SALARY integer NOT NULL,
     DEPARTMENT_ID integer NOT NULL)
     """

cur.execute(employee)
employee_new = """
     ALTER TABLE employee
     ADD COLUMN CITY text NOT NULL
     """
cur.execute(employee_new)
emp1 = create_employee(con,1,'Esha',2000,4,'TRV')
emp2 = create_employee(con,2,'Amit',1000,7,'MUM')
emp3 = create_employee(con,3,'Ulma',3000,5,'TRV')
emp4 = create_employee(con,4,'Desh',4000,6,'DEL')
emp5 = create_employee(con,5,'John',5000,8,'TRV')
con.commit()
cursor = con.execute("SELECT * from employee")
records = cursor.fetchall()
for row in records:
    print("NAME: " + str(row[1]) + "   ID : " + str(row[0]) + "   SALARY: " + str(row[2]))
departments = """
     CREATE TABLE departments (
     DEPARTMENT_ID integer NOT NULL,
     DEPARTMENT_NAME text NOT NULL,
     FOREIGN KEY (DEPARTMENT_ID) references employee (DEPARTMENT_ID))
     """

cur.execute(departments)
dep1 = create_department(con,4,'Sales')
dep2 = create_department(con,5,'Management')
dep3 = create_department(con,6,'Development')
dep4 = create_department(con,7,'UI/UX')
dep5 = create_department(con,8,'Testing')
con.commit()

while True:
    a = input("Please choose a the fisrt letter of employee name whose detail you want to be printed(please choose from (E,A,U,D,J)): ")
    par1 = [a + '%']
    if a not in ['E','A','U','D','J']:
        print("No employee with the chosen first letter, please choose from (E,A,U,D,J)")
    else:
        SQL_1 = "SELECT * from employee where NAME LIKE ?"
        records=cur.execute(SQL_1,par1)
        for i in records:
            print('ID: ' + str(i[0]) + '---NAME: ' + i[1] + '---SALARY: ' + str(i[2]) + '---DEPARTMENT_ID: ' + str(i[3]) + '---CITY: ' + i[4])
    b = input("Enter a ID to see details: ")
    if int(b) in [1,2,3,4,5]:
        SQL_2 = "SELECT * FROM employee where ID = ? "
        records2=cur.execute(SQL_2, b)
        for i in records2:
            print('ID: ' + str(i[0]) + '---NAME: ' + i[1] + '---SALARY: ' + str(i[2]) + '---DEPARTMENT_ID: ' + str(i[3]) + '---CITY: ' + i[4])
    else:
        print("Invalid ID, please choose 1,2,3,4 or 5")
    c = input("Enter new name chosen by you:")
    para = c,b
    SQL_3 = "UPDATE employee SET NAME = ? WHERE ID = ?"
    cur.execute(SQL_3,para)
    SQL_5 = "SELECT * FROM employee"
    records3 = cur.execute(SQL_5)
    print("------TABLE CHANGED WITH NEW NAME BY USER-------")
    for i in records3:
        print('ID: ' + str(i[0]) + '---NAME: ' + i[1] + '---SALARY: ' + str(i[2]) + '---DEPARTMENT_ID: ' + str(
            i[3]) + '---CITY: ' + i[4])
    pare = input("Enter department_id to see all the details of employees in that department (choose from:4/5/6/7/8)")
    sql_4 = "SELECT * from employee,departments where employee.DEPARTMENT_ID = departments.DEPARTMENT_ID and departments.DEPARTMENT_ID = ?"
    records5=cur.execute(sql_4,pare)
    for i in records3:
        print('ID: ' + str(i[0]) + '---NAME: ' + i[1] + '---SALARY: ' + str(i[2]) + '---DEPARTMENT_ID: ' + str(
            i[3]) + '---CITY: ' + i[4]  + '---DEPARTMENT: ' + i[6])
    yes = input("Do you want to ask again? y/n:")
    if yes == 'y':
        continue
    else:
        break
con.commit
con.close()
