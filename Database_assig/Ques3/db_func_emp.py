# db_utils.py
import os
import sqlite3

# create a default path to connect to and create (if necessary) a database
# called 'database.sqlite3' in the same directory as this script
DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'database.sqlite3')

def db_connect(db_path=DEFAULT_PATH):
    con = sqlite3.connect(db_path)
    return con

def create_employee(con, ID, NAME, SALARY, DEPARTMENT_ID,CITY):
    sql = """
        INSERT INTO employee (ID, NAME, SALARY, DEPARTMENT_ID,CITY)
        VALUES (?, ?,?,?,?)
        """
    cur = con.cursor()
    cur.execute(sql, (ID, NAME, SALARY, DEPARTMENT_ID,CITY))
    return cur.lastrowid

def create_department(con,DEPARTMENT_ID,DEPARTMENT_NAME):
    sql = """
        INSERT INTO departments (DEPARTMENT_ID,DEPARTMENT_NAME)
        VALUES (?, ?)
        """
    cur = con.cursor()
    cur.execute(sql, (DEPARTMENT_ID,DEPARTMENT_NAME))
    return cur.lastrowid





