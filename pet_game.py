import random

class Pet:
    sound = ["meow"]
    state = ["happy","bored","hungry"]
    ranges_to_choose = list(range(10))
    decrements = 4
    def __init__(self,name):
        self.name = name
        self.hungry = random.choice(self.ranges_to_choose)
        self.boredom = random.choice(self.ranges_to_choose)
        self.sound = self.sound[:]

    def clock_tick(self):
        self.boredom += 1
        self.hungry += 1

    def state_pet(self):

        if self.hungry > 6:
            return self.state[2]
        if self.boredom > 6:
            return self.state[1]
        if self.boredom <= 6 and self.hungry <= 6:
            return self.state[0]

    def __str__(self):
        output_state_name = "Hi, I am " + str(self.name) + " and I am " + str(self.state_pet())
        return output_state_name

    def reduce_boredom(self):
        self.boredom = max(0, self.boredom - self.decrements)
        print("boredom score of " + str(self.name) + " " +  str(self.boredom))

    def reduce_hunger(self):
        self.hungry = max(0, self.hungry - self.decrements)
        print("hungry score of" + str(self.name) + str(self.hungry))

    def teach(self,new_word):
        self.sound.append(new_word)
        self.reduce_boredom()

    def hi(self):
        D = "Hey listen to me say " + str(random.choice(self.sound))
        self.reduce_boredom()
        print(D)
        return D

    def feed(self):
        self.reduce_hunger()

instructions = print("-----Welcome to the game-----.\n-----How to play?-----\n1. \"Adopt\" a pet and give a \"name\" of your choice\n2. \"Feed\" if the pet says hungry \n3. \"Teach\" if the pet says bored \n4. Or let the pet \"Talk\" if it says bored \n5. \"Exit\" to stop playing")

def pet_select(name_select, name):
    for pet in name_select:
        if pet.name == name:
            return pet
    return None

def game():

    animals = []
    input_command = ""
    name_by_user = ""
    while True:
        action = input("Adopt <give a single word name>\nTalk <name of your pet>\nTeach <name of your pet> <single word to teach>\nFeed <name of your pet>\nExit Game\nChoose a command from above:" + input_command + "\n" + name_by_user)
        input_command = ""
        split_input = action.split()
        if len(split_input) > 0:
            command = split_input[0]
        else:
            command = None
        if command == "Exit Game":
            print("Game Exited")
        elif command == "Adopt" and len(split_input) > 1:
            if pet_select(animals, split_input[1]):
                input_command += "That pet name exists, please select another name\n"
            else:
                animals.append(Pet(split_input[1]))
        elif command == "Talk" and len(split_input) > 1:
            pet = pet_select(animals, split_input[1])
            if not pet:
                input_command += "Wrong pet name, please enter correct name.\n"
                print()
            else:
                pet.hi()
        elif command == "Teach" and len(split_input) > 2:
            pet = pet_select(animals, split_input[1])
            if not pet:
                input_command += "Wrong pet name, please enter correct name"
            else:
                pet.teach(split_input[2])
        elif command == "Feed" and len(split_input) > 1:
            pet = pet_select(animals, split_input[1])
            if not pet:
                input_command += "Wrong pet name, please enter correct name."
            else:
                pet.feed()
        else:
            input_command+= "Invalid format of input or you forgot to enter the word"

        for pet in animals:
            pet.clock_tick()
            input_command += "\n" + pet.__str__()
game()


